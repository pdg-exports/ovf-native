# OVF Native App

# Project

Native application for Ocean View Flowers is designed to deliver website functionality and extend it in on a Native platform for Android and IOS. The app is build using Expo SDK. Expo is convenient package for Apps build with React Native includes many useful development tools. Its

# How to start and run

1. Pull repo from the hub;
2. Run `npm i` or `yarn install` terminal, inside repo folder (must have node.js with npm or yarn installed)
3. Important to separately install Android studio for Android device emulator and in browser dev tools, and XCODE for ios emulators. 
4. After installing all dependancies, use `npm run start` or `yarn start` to start the metro bundler in dev mode.

# Dependencies

- expo- includes many expo modules, like gesture-handler, icons, and a lot more;
- expo-sqlite - runs sql like database with easy JS api;
- react - yup;
- react-native - React Native version pulled from expo sdk repo;
- react-navigation - manages and has components for different types of navigation in native apps;
- redux -manages global state for the app. Welcome to hell;
- react-redux - connects redux store with react components;
- reselect - helper library to access certain parts of state;
- styled-components/native - CSS like styling for native components.

All packages have very good documentation, support, and community.

# Composition

Repo root folder contains app.json and App.js which are, in combination, the entry point for an application. App.js is tasked with showing the loading screen, while an app loads data from api, fonts, creates store. Once the async process is done , it renders main OVFapp component. Inside `src` folder, the application divided in four types of components: Components - includes class and functional react components, Screens - equivalent of pages for web top of the chain components connected with navigation, State - folder that contains methods to create, manage and influence global state of the app. Util - helper functions to work with data from api, db etc. etc. 

# Components

**Library** - folder with simple reusable components that makes life easier. App Theme object is here that controls app typography, color palette and other styles. Components in Library folder are Molecules from Atomic design that combine few Atoms (components imported from react native). The are stateless with the exception of `Tabs`.

Other components inside `Components` folder are Organisms that are intended for use as part of certain `Screen`. First two letters in the name of the component signifying which screen it relates to. `PS` - ProductSolo, `PA` - Product Archive, `HM` - Home, `INF` - Info Screen.

# State

Store consists of several reducers: `filter` as name implies holds object which stores filters that are applied to products, `gallery` reducer takes care of interactions of image carousel on Single Products screen. `products` holds data for all products, `categories` - data for categories, `colors` - data for colors and `availability`  - data for availability category.

# Helpful resources

**Expo:** [https://docs.expo.io/versions/latest/](https://docs.expo.io/versions/latest/)

**React Native**: [https://facebook.github.io/react-native/docs/getting-started](https://facebook.github.io/react-native/docs/getting-started)

**React Navigation**: [https://reactnavigation.org/docs/en/getting-started.html](https://reactnavigation.org/docs/en/getting-started.html)

**Redux**: [https://redux.js.org/introduction/getting-started](https://redux.js.org/introduction/getting-started)

### GL HF