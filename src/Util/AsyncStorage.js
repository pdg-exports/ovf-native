import { AsyncStorage } from "react-native"

const error = {
  get: "STORAGE GET ERROR",
  set: "STORAGE SET ERROR"
}

export const getStorageData = async key => {
  try {
    let data = await AsyncStorage.getItem(key)
    if (data === null) {
      return undefined
    }
    return JSON.parse(data)
  } catch (err) {
    console.error(error.get, err)
  }
}

export const setStorageData = async (key, data) => {
  try {
    let retireved = await AsyncStorage.setItem(key, JSON.stringify(data))
    if (retireved === null) {
      return undefined
    }
    return retireved
  } catch (err) {
    console.error(error.set, err)
  }
}
