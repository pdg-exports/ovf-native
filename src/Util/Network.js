const URL = "https://oceanviewflowers.press2.pagedesign.us/wp-json/wp/v2/"
const PING_URL =
  "https://api.oceanviewflowers.com/wp-json/nativeapp/v1/ping-pong"

const netError = {
  get: "NETWORK GET ERROR"
}

export const getApiData = async path => {
  try {
    const data = await (await fetch(URL + path)).json()
    return data
  } catch (err) {
    console.log(netError.get, err)
  }
}

export const pingServer = async () => {
  let startDate = new Date()
  let endDate = null
  try {
    const data = await (await fetch(PING_URL)).json()
    if (data === true) {
      endDate = new Date()
      return endDate.getTime() - startDate.getTime()
    }
    return false
  } catch (err) {
    console.log(netError.get, err)
    return false
  }
}
