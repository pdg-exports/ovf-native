export function inArrayField(array, value) {
  for (let i = 0; i < array.length; ++i) {
    if (array[i].slug === value) {
      return true
    }
  }
  return false
}

export function extractInArrayField(array, value) {
  for (let i = 0; i < array.length; ++i) {
    if (array[i].slug.substring(0, 3) === value) {
      return true
    }
  }
  return false
}
