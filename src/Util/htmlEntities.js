export function decodeHtmlEntity(string) {
  return string.replace(/&#(\d+);/g, function(match, dec) {
    return String.fromCharCode(dec)
  })
}

export function encodeHtmlEntity(string) {
  var buf = []
  for (var i = string.length - 1; i >= 0; i--) {
    buf.unshift(["&#", string[i].charCodeAt(), ";"].join(""))
  }
  return buf.join("")
}
