import { SQLite } from "expo-sqlite"

const db = SQLite.openDatabase("ovf.db")

db.transaction(
  tx => {
    tx.executeSql(
      "create table if not exists products (id integer primary key not null, slug text, name text);"
    )
    tx.executeSql(
      "insert into products (id,slug,name) values (1,'shit', 'test')"
    )
    tx.executeSql("select * from products", [], (_, { rows }) =>
      console.log("wow", JSON.stringify(rows))
    )
  },
  err => {
    console.warn("SQL query error =>", err)
  }
)
