import React from "react"
import {
  TouchableOpacity,
  TouchableNativeFeedback,
  Platform
} from "react-native"

const TouchEffect = ({
  children,
  onPress,
  onLongPress,
  color,
  borderless = false,
  ...other
}) => {
  if (Platform.OS === "android") {
    return (
      <TouchableNativeFeedback
        {...other}
        onPress={onPress}
        onLongPress={onLongPress}
        useForeground={true}
        background={TouchableNativeFeedback.Ripple(color, false)}
      >
        {children}
      </TouchableNativeFeedback>
    )
  } else {
    return (
      <TouchableOpacity
        activeOpacity={0.2}
        onPress={onPress}
        onLongPress={onLongPress}
        {...other}
      >
        {children}
      </TouchableOpacity>
    )
  }
}

TouchEffect.defaultProps = {
  color: `rgba(0,0,0,.3)`
}

export default TouchEffect
