import React from "react"
import TouchEffect from "../TouchEffect"
import styled from "styled-components/native"
import { H4 } from "../Typography"
import { MaterialCommunityIcons } from "@expo/vector-icons"
import Theme from "../Theme"

const ListContainer = styled.View`
  height: 60px;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  width: 100%;
  padding: 3px 8px;
  border-top-width: 1px;
  border-color: rgba(0, 0, 0, 0.2);
`

const ListImage = styled.Image`
  resize-mode: cover;
  width: 53px;
  height: 100%;
`
const ListTextContainer = styled.View`
  flex: 1;
  margin-left: 10px;
`
const ListCard = props => {
  const { onPress, onLongPress, source, title } = props
  return (
    <TouchEffect onPress={onPress} onLongPress={onLongPress}>
      <ListContainer>
        <ListImage
          source={source}
          resizeMode="contain"
          progressiveRenderingEnabled
        />
        <ListTextContainer>
          <H4 align="left">{title}</H4>
        </ListTextContainer>
        <MaterialCommunityIcons
          name="heart-outline"
          size={28}
          iconStyle={{ marginRight: 0, marginLeft: 0 }}
          color={Theme.palette.success}
          backgroundColor="#fff"
        />
      </ListContainer>
    </TouchEffect>
  )
}

export default React.memo(ListCard)
