import React from "react"
import { View } from "react-native"
import styled from "styled-components/native"
import Tab from "./Tab"

const STabContainer = styled.View`
  flex-direction: row;
  align-items: stretch;
  width: 100%;
`

class Tabs extends React.Component {
  state = {
    activeTab: this.props.children[0].props.label
  }

  onPressTab = tab => {
    this.setState({ activeTab: tab })
  }

  render() {
    const { activeTab } = this.state
    const { children, ...other } = this.props
    return (
      <View style={{ height: "100%" }}>
        <STabContainer>
          {React.Children.map(children, c => {
            const { label } = c.props
            return (
              <Tab
                active={label === activeTab}
                key={label}
                onPress={this.onPressTab}
                label={label}
              />
            )
          })}
        </STabContainer>
        <View>
          {React.Children.map(children, c => {
            const { label } = c.props
            if (label === activeTab) return c.props.children
            return null
          })}
        </View>
      </View>
    )
  }
}

export default Tabs
