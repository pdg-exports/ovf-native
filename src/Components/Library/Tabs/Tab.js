import React from "react"
import styled from "styled-components/native"
import TouchEffect from "../TouchEffect"
import { H5 } from "../Typography"

const StyledTab = styled.View`
  padding: 10px 12px;
  border-bottom-width: 4px;
  flex-grow: 1;
  border-bottom-color: ${props =>
    props.active ? "#B3C575" : props.theme.palette.secondary};
`

const Tab = props => {
  const { label, active, onPress } = props
  return (
    <TouchEffect onPress={() => onPress(props.label)} style={{ flex: 1 }}>
      <StyledTab active={active}>
        <H5 color="#fff">{label}</H5>
      </StyledTab>
    </TouchEffect>
  )
}

export default Tab
