import React from "react"
import { Dimensions } from "react-native"
import TouchEffect from "../TouchEffect"
import styled from "styled-components/native"
import { MaterialCommunityIcons } from "@expo/vector-icons"
import Theme from "../Theme"

const dimensions = Dimensions.get("window")

const CardContainer = styled.View`
  position: relative;
  shadow-color: #000;
  shadow-offset: 0px 2px;
  shadow-opacity: 0.5;
  shadow-radius: 2;
  elevation: 3;
  width: ${(dimensions.width - 50) / 2}px;
  margin: 10px;
  border-radius: 0px;
`
const CardImage = styled.Image`
  height: 177px;
  width: 100%;
  resize-mode: cover;
`

const CardTextContainer = styled.View`
  padding: 10px 15px;
  background: #ffffff;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`

const CardText = styled.Text`
  font-family: nunito-bold;
  font-size: ${props => props.theme.typography.fontSize.h5};
  flex: 1;
  flex-wrap: wrap;
`

const TileCard = props => {
  const { onPress, onLongPress, source, title } = props
  return (
    <TouchEffect onPress={onPress} onLongPress={onLongPress}>
      <CardContainer borderRadius={10}>
        <CardImage
          source={source}
          resizeMode="cover"
          progressiveRenderingEnabled
        />
        <CardTextContainer>
          <CardText>{title}</CardText>
          <MaterialCommunityIcons
            name="heart-outline"
            size={20}
            iconStyle={{ marginRight: 0, marginLeft: 0 }}
            color={Theme.palette.primary}
            backgroundColor="#fff"
          />
        </CardTextContainer>
      </CardContainer>
    </TouchEffect>
  )
}

export default React.memo(TileCard)
