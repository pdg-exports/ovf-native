import React from "react"
import {
  View,
  TouchableOpacity,
  TouchableNativeFeedback,
  Platform
} from "react-native"

import styled from "styled-components/native"
import TouchEffect from "../TouchEffect"

const ButtonWrapper = styled.View`
  padding: 12px;
  border-radius: ${props => props.theme.borderRadius};
  background-color: ${props => {
    const { variant, theme, color } = props
    switch (variant) {
      case "filled":
        return theme.palette[color]
      default:
        return "transparent"
    }
  }};
  border: ${props => {
    const { variant, theme, color } = props
    if (variant === "outlined") {
      return "1px solid " + theme.palette[color]
    }
    return "transparent"
  }};
`
const ButtonText = styled.Text`
  color: ${props => {
    const { variant, theme, color } = props
    switch (variant) {
      case "outlined":
        return theme.palette[color]
      default:
        return "#ffffff"
    }
    return theme.typography.fontColor
  }};
  font-weight: ${props => props.theme.typography.fontWeight.bold};
  text-align: center;
  font-size: ${props => {
    const { theme, fontSize } = props
    if (fontSize) {
      return fontSize
    }
    return theme.typography.fontSize.base
  }};
`

const Button = props => {
  const {
    onPress,
    effectColor,
    children,
    variant,
    theme,
    color,
    ...other
  } = props
  const styleProps = { variant, theme, color }
  return (
    <TouchEffect onPress={onPress} color={effectColor}>
      <ButtonWrapper {...styleProps}>
        <ButtonText {...styleProps}>{children.toUpperCase()}</ButtonText>
      </ButtonWrapper>
    </TouchEffect>
  )
}

export default Button
