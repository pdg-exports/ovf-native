const Theme = {
  palette: {
    primary: "#6b863a",
    secondary: "#4f0236",
    success: "#b3c575",
    warning: "#eec24c",
    light: "#f7f7f7",
    dark: "#360526",
    white: "#fff",
    grey: "#575757",
    ltgrey: "#b2b2b2",
    ltgreen: "#EDF3D9"
  },
  typography: {
    fontColor: "#000000",
    fontFamily: "",
    fontSize: {
      base: 14,
      lg: 16,
      sm: 12,
      h1: 34,
      h2: 28,
      h3: 22,
      h4: 18,
      h5: 16,
      h6: 16
    },
    fontWeight: {
      light: 300,
      normal: 400,
      bold: 700
    }
  },
  shadow: "0 14px 28px rgba(#000, 0.25), 0 10px 10px rgba(#000, 0.22)",
  borderRadius: 0,
  spacer: "1rem",
  colorChange: "15%",
  lineHeight: "1.5px",
  transitionBase: "all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1)"
}

export default Theme
