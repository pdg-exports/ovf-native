import React from "react"
import { Platform } from "react-native"
import { SafeAreaView } from "react-navigation"

const SafeArea = props => {
  const { children, style, ...other } = props
  return (
    <SafeAreaView
      style={{
        flex: 1,
        paddingTop: Platform.OS === "android" ? 25 : 0,
        ...style
      }}
      {...other}
    >
      {children}
    </SafeAreaView>
  )
}

export default SafeArea
