import React from "react"
import styled from "styled-components/native"

export const H1 = styled.Text`
  color: ${props =>
    props.color ? props.color : props.theme.typography.fontColor};
  font-family: "nunito-bold";
  text-align: ${props => props.align || "center"};
  font-size: ${props => props.theme.typography.fontSize.h1};
  margin-bottom: ${props => (props.gutterBottom ? "15px" : "0px")};
`

export const H2 = styled.Text`
  color: ${props =>
    props.color ? props.color : props.theme.typography.fontColor};
  font-family: "nunito-bold";
  text-align: ${props => props.align || "center"};
  font-size: ${props => props.theme.typography.fontSize.h2};
  margin-bottom: ${props => (props.gutterBottom ? "13px" : "0px")};
`
export const H3 = styled.Text`
  color: ${props =>
    props.color ? props.color : props.theme.typography.fontColor};
  font-family: "nunito-bold";
  text-align: ${props => props.align || "center"};
  font-size: ${props => props.theme.typography.fontSize.h3};
  margin-bottom: ${props => (props.gutterBottom ? "10px" : "0px")};
`
export const H4 = styled.Text`
  color: ${props =>
    props.color ? props.color : props.theme.typography.fontColor};
  font-family: "nunito-bold";
  text-align: ${props => props.align || "center"};
  font-size: ${props => props.theme.typography.fontSize.h4};
  margin-bottom: ${props => (props.gutterBottom ? "10px" : "0px")};
`
export const H5 = styled.Text`
  color: ${props =>
    props.color ? props.color : props.theme.typography.fontColor};
  font-family: "nunito-bold";
  text-align: ${props => props.align || "center"};
  font-size: ${props => props.theme.typography.fontSize.h5};
  margin-bottom: ${props => (props.gutterBottom ? "10px" : "0px")};
`
export const P = styled.Text`
  color: ${props =>
    props.color ? props.color : props.theme.typography.fontColor};
  font-family: "nunito-semi";
  text-align: ${props => props.align || "center"};
  font-size: ${props => props.theme.typography.fontSize.base};
  margin-bottom: ${props => (props.gutterBottom ? "8px" : "0px")};
  text-transform: ${props => (props.capitalize ? "capitalize" : "none")};
`
