import React from "react"
import { View, ScrollView } from "react-native"
import { connect } from "react-redux"
import { jumpTo } from "../../State/actions"
import { TouchEffect } from "../Library"
import { H4 } from "../Library/Typography"
import styled from "styled-components/native"

const StyledCircle = styled.View`
  width: 48px;
  height: 48px;
  background: ${props => (props.color ? props.color : "#000")};
  margin-right: 10px;
  margin-bottom: 10px;
  border: 1px solid rgba(0, 0, 0, 0.25);
`

function PSColor(props) {
  const { variations, jumpTo } = props
  let temp = variations.map(e => e.images && e.images.length) || null
  return (
    <View>
      <ScrollView horizontal={true}>
        <View
          style={{ display: "flex", flexDirection: "row", flexWrap: "wrap" }}
        >
          {variations &&
            variations.map((el, i) => (
              <TouchEffect
                key={el.color_name}
                onPress={() => {
                  jumpTo(temp.slice(0, i).reduce((acc, cur) => acc + cur, 0))
                }}
                borderless={true}
              >
                <StyledCircle color={el.teeth && el.teeth[0].color} />
              </TouchEffect>
            ))}
        </View>
      </ScrollView>
    </View>
  )
}

export default connect(
  null,
  { jumpTo }
)(PSColor)
