import React from "react"
import styled from "styled-components/native"

export default (PickerContainer = styled.View`
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: ${props => (props.around ? "space-around" : "flex-start")};
`)
