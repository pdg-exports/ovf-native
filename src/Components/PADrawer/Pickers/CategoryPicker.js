import React from "react"
import { Image } from "react-native"
import PickerContainer from "./PickerContainer"
import { connect } from "react-redux"
import { replaceFilter } from "../../../State/actions/filter"
import { TouchEffect } from "../../Library"

const ICON_SRC = "../../../../assets/icons/categories/"
const ICON_EXT = ".png"

const localIcons = {
  botanical: require(ICON_SRC + "botanical" + ICON_EXT),
  filler: require(ICON_SRC + "filler" + ICON_EXT),
  foliage: require(ICON_SRC + "foliage" + ICON_EXT),
  form: require(ICON_SRC + "form" + ICON_EXT),
  line: require(ICON_SRC + "line" + ICON_EXT),
  mass: require(ICON_SRC + "mass" + ICON_EXT)
}

const CategoryPicker = props => {
  const fname = "category"
  const { categories, replaceFilter } = props
  return (
    <PickerContainer>
      {categories.map(({ name, slug }) => {
        return (
          <TouchEffect
            key={slug}
            onPress={() => {
              replaceFilter(fname, slug)
            }}
            borderless={true}
          >
            <Image
              source={localIcons[slug]}
              fadeDuration={0}
              style={{ width: 60, height: 60, margin: 5 }}
            />
          </TouchEffect>
        )
      })}
    </PickerContainer>
  )
}

export default connect(
  state => ({
    filter: state.filter,
    categories: state.categories
  }),
  { replaceFilter }
)(CategoryPicker)
