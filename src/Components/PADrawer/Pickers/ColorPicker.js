import React from "react"
import { connect } from "react-redux"
import { replaceFilter } from "../../../State/actions/filter"
import { TouchEffect } from "../../Library"
import { View } from "react-native"
import styled from "styled-components/native"
import PickerContainer from "./PickerContainer"

const StyledCircle = styled.View`
  width: 38px;
  height: 38px;
  margin: 5px;
  overflow: hidden;
  transform: scale(${props => (props.active ? "1.2" : "1")});
  border: 1px solid rgba(0, 0, 0, 0.4);
  background-color: ${props => props.color};
`

const ColorPicker = props => {
  const fname = "color"
  const { filter, replaceFilter, colors } = props
  return (
    <PickerContainer>
      {colors.map(({ slug, acf }) => {
        return (
          <TouchEffect
            onPress={() => {
              replaceFilter(fname, slug)
            }}
            borderless={true}
            key={slug}
          >
            <StyledCircle
              active={filter.color === slug ? true : false}
              color={acf.color_display}
            />
          </TouchEffect>
        )
      })}
    </PickerContainer>
  )
}

export default connect(
  state => ({
    filter: state.filter,
    colors: state.colors
  }),
  { replaceFilter }
)(ColorPicker)
