import React from "react"
import { View, Text } from "react-native"
import { connect } from "react-redux"
import styled from "styled-components/native"
import { replaceFilter } from "../../../State/actions/filter"
import { TouchEffect } from "../../Library"
import PickerContainer from "./PickerContainer"

const months = [
  "jan",
  "feb",
  "mar",
  "apr",
  "may",
  "jun",
  "jul",
  "aug",
  "sep",
  "oct",
  "nov",
  "dec"
]

const Boxie = styled.View`
  border: 1px solid #000;
  padding: 2px 4px;
  margin: 5px;
`
function MonthsPicker({ filter, replaceFilter }) {
  const fname = "month"
  return (
    <PickerContainer>
      {months.map(el => {
        return (
          <TouchEffect
            key={el}
            onPress={() => {
              replaceFilter(fname, el)
            }}
          >
            <Boxie>
              <Text>{el.toUpperCase()}</Text>
            </Boxie>
          </TouchEffect>
        )
      })}
    </PickerContainer>
  )
}

export default connect(
  state => ({
    filter: state.filter
  }),
  { replaceFilter }
)(MonthsPicker)
