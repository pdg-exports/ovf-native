import React from "react"
import { View, Text, Dimensions } from "react-native"
import DrawerLayout from "react-native-gesture-handler/DrawerLayout"
import { MaterialCommunityIcons } from "@expo/vector-icons"
import { H2, H5 } from "../Library/Typography"
import { SafeArea } from "../Library"
import { ColorPicker, CategoryPicker, MonthsPicker } from "./Pickers"

const dimensions = Dimensions.get("window")

class PADrawer extends React.Component {
  _openDrawer = () => {
    this.drawer.openDrawer()
  }

  _closeDrawer = () => {
    this.drawer.closeDrawer()
  }

  _renderDrawerContent = () => {
    return (
      <SafeArea>
        <View
          style={{
            paddingLeft: 10,
            paddingRight: 10,
            paddingTop: 20
          }}
        >
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center"
            }}
          >
            <H2 gutterButtom>Filter</H2>
            <MaterialCommunityIcons.Button
              borderRadius={0}
              name="window-close"
              size={32}
              iconStyle={{ marginRight: 0, marginLeft: 0 }}
              color="#4f4f4f"
              backgroundColor="#fff"
              onPress={this._closeDrawer}
            />
          </View>
          <H5 align="left" gutterButtom>
            CATEGORY
          </H5>
          <CategoryPicker />
          <H5 align="left" gutterButtom>
            COLOR
          </H5>
          <ColorPicker />
          <H5 align="left" gutterButtom>
            AVAILABILITY
          </H5>
          <MonthsPicker />
        </View>
      </SafeArea>
    )
  }
  render() {
    const { children } = this.props
    return (
      <DrawerLayout
        drawerWidth={dimensions.width * 0.75}
        drawerPosition={DrawerLayout.positions.Right}
        drawerType="front"
        drawerBackgroundColor="#fff"
        renderNavigationView={this._renderDrawerContent}
        edgeWidth={100}
        ref={_drawer => (this.drawer = _drawer)}
      >
        {React.Children.map(children, c => {
          const { controller } = c.props
          if (controller)
            return React.cloneElement(c, {
              toggleDrawer: this._openDrawer
            })
          return c
        })}
      </DrawerLayout>
    )
  }
}

export default PADrawer
