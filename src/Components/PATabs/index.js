import React, { useState } from "react"
import styled from "styled-components/native"
import Tab from "../Library/Tabs/Tab"

const STabContainer = styled.View`
  flex-direction: row;
  align-items: stretch;
  width: 100%;
`

const PATabs = props => {
  const [fav, setFav] = useState(false)
  return (
    <STabContainer>
      <Tab
        label="All Flowers"
        active={!fav}
        onPress={() => {
          setFav(false)
        }}
      />
      <Tab
        label="My Favorites"
        active={fav}
        onPress={() => {
          setFav(true)
        }}
      />
    </STabContainer>
  )
}

export default PATabs
