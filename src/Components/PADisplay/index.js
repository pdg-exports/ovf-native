import React from "react"
import { Dimensions, View } from "react-native"
import { FlatList } from "react-native-gesture-handler"
import * as Select from "../../State/select"
import { connect } from "react-redux"
import { TileCard, ListCard, Theme } from "../Library"
import { decodeHtmlEntity } from "../../Util/htmlEntities"
import { MaterialCommunityIcons } from "@expo/vector-icons"
import PAFilters from "../PAFilters"
import fFallback from "../../../assets/img/fallbacks/product.jpg"

const dimensions = Dimensions.get("window")

class PADisplay extends React.Component {
  state = {
    listView: false
  }

  _changeView = () => {
    this.setState(prev => ({ listView: !prev.listView }))
  }

  render() {
    const { products, navigation, toggleDrawer, ...other } = this.props
    const { listView } = this.state
    return (
      <View style={{ backgroundColor: "#fff", flex: 1 }}>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
            paddingLeft: 10,
            paddingRight: 10
          }}
        >
          <PAFilters />
          <View style={{ flexDirection: "row" }}>
            <MaterialCommunityIcons.Button
              borderRadius={0}
              name={listView ? "view-grid" : "view-list"}
              size={36}
              iconStyle={{ marginRight: 0, marginLeft: 0 }}
              color={Theme.palette.primary}
              backgroundColor="#fff"
              onPress={this._changeView}
            />

            <MaterialCommunityIcons.Button
              name="filter"
              borderRadius={0}
              size={36}
              color={Theme.palette.primary}
              iconStyle={{ marginRight: 0, marginLeft: 0 }}
              backgroundColor="#fff"
              onPress={toggleDrawer}
            />
          </View>
        </View>
        <FlatList
          style={{
            flex: 1,
            width: dimensions.width,
            height: "100%"
          }}
          contentContainerStyle={{
            paddingLeft: 5,
            paddingRight: 5
          }}
          numColumns={listView ? 1 : 2}
          key={listView ? 1 : 0}
          data={products}
          extraData={products}
          keyExtractor={item => item.id.toString()}
          renderItem={({ item }) =>
            listView ? (
              <ListCard
                key={item.id}
                source={
                  item.featured_image_mobile
                    ? { uri: item.featured_image_mobile }
                    : fFallback
                }
                title={decodeHtmlEntity(item.title.rendered)}
                onPress={() =>
                  navigation.navigate("ProductSolo", {
                    id: item.id,
                    title: item.title.rendered
                  })
                }
              />
            ) : (
              <TileCard
                key={item.id}
                source={
                  item.featured_image_mobile
                    ? { uri: item.featured_image_mobile }
                    : fFallback
                }
                onPress={() =>
                  navigation.navigate("ProductSolo", {
                    id: item.id,
                    title: item.title.rendered
                  })
                }
                title={decodeHtmlEntity(item.title.rendered)}
              />
            )
          }
        />
      </View>
    )
  }
}

export default connect(state => ({
  products: Select.getFilteredResults(state)
}))(PADisplay)
