import React from "react"
import { View } from "react-native"
import { connect } from "react-redux"
import { MaterialCommunityIcons } from "@expo/vector-icons"
import styled from "styled-components/native"
import { Theme, TouchEffect } from "../Library"
import { P } from "../Library/Typography"
import { removeFilter } from "../../State/actions"

const StyledPill = styled.View`
  padding: 3px 6px;
  margin: 2px;
  background-color: ${props => props.theme.palette.ltgreen};
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  border-radius: 5px;
`

function PAFilters(props) {
  const { filter, remove } = props
  const filterValues = Object.values(filter)
  const filterNames = Object.keys(filter)
  return (
    <View style={{ flexDirection: "row" }}>
      {filterValues.map((el, i) => (
        <TouchEffect key={el} onPress={() => remove(filterNames[i])}>
          <StyledPill>
            <P align="left" color={Theme.palette.primary} capitalize>
              {el}
            </P>
            <MaterialCommunityIcons
              borderRadius={0}
              name="window-close"
              size={20}
              iconStyle={{
                marginRight: 0,
                marginLeft: 0,
                marginTop: 0,
                marginBottom: 0,
                paddingLeft: 0,
                paddingRight: 0
              }}
              color={Theme.palette.primary}
              backgroundColor={Theme.palette.ltgreen}
            />
          </StyledPill>
        </TouchEffect>
      ))}
    </View>
  )
}

export default connect(
  state => ({ filter: state.filter }),
  { remove: removeFilter }
)(PAFilters)
