import React from "react"
import { View } from "react-native"
import styled from "styled-components/native"
import { P } from "../Library/Typography"

const StyledBadge = styled.View`
  padding: 4px 8px;
  margin: 2px;
  background-color: ${props =>
    props.avail === "available"
      ? props.theme.palette.success
      : props.avail === "limited"
      ? props.theme.palette.warning
      : "none"};
`

const PSAvailablity = ({ avail }) => {
  return (
    <View>
      <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
        {avail.map(m => {
          const c = m.name.split(",")
          const t = c[1].trim()
          const a = c[2].trim()
          return (
            <StyledBadge key={m.term_id} avail={a}>
              <P>{t.toUpperCase()}</P>
            </StyledBadge>
          )
        })}
      </View>
    </View>
  )
}

export default PSAvailablity
