import React, { useEffect, useState } from "react"
import { connect } from "react-redux"
import SwipeableViews from "react-swipeable-views-native"
import { forwardItem, backItem, jumpTo, reset } from "../../State/actions"
import styled from "styled-components/native"
import { View, Text, Dimensions } from "react-native"
import Pagination from "./Pagination"
import fFallback from "../../../assets/img/fallbacks/product.jpg"

const page = number => {
  if (number < 10) {
    return `0${number}`
  }
  return number
}

const SSoloImg = styled.Image`
  width: 100%;
  height: 600px;
`

function PSCarousel(props) {
  const {
    variations,
    category,
    gallery,
    forwardItem,
    backItem,
    jumpTo,
    reset
  } = props

  const [totalItems, setTotalItems] = useState(0)
  var { height, width } = Dimensions.get("window")
  useEffect(() => {
    let temp = 0
    variations.forEach(element => {
      if (element.images) temp += element.images.length
    })
    setTotalItems(temp)

    return () => {
      reset()
    }
  }, [variations])

  return (
    <View style={{ height: "100%" }}>
      <SwipeableViews
        index={gallery}
        onChangeIndex={index => jumpTo(index)}
        style={{ height: "100%" }}
      >
        {variations.map(
          variation =>
            variation.images &&
            variation.images.map(image => (
              <View style={{ overflow: "hidden", height: "100%" }}>
                <SSoloImg
                  source={
                    image.sizes ? { uri: image.sizes.medium_large } : fFallback
                  }
                  resizeMode="cover"
                />
              </View>
            ))
        )}
      </SwipeableViews>
      <View
        style={{
          position: "absolute",
          bottom: 0,
          right: 0,
          padding: 15,
          backgroundColor: "#fff"
        }}
      >
        <Text>
          {page(gallery + 1)} &mdash;&mdash; {page(totalItems)}
        </Text>
      </View>
    </View>
  )
}

export default connect(
  state => ({
    gallery: state.gallery
  }),
  { backItem, forwardItem, jumpTo, reset }
)(PSCarousel)
