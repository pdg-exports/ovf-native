import React from "react"
import { View } from "react-native"
import styled from "styled-components/native"
// STYLED BEGIN
const SPaginationContainer = styled.View`
  position: absolute;
  bottom: 10px;
  display: flex;
  flex-direction: row;
  align-self: center;
`
const SDot = styled.View`
  height: 12px;
  width: 12px;
  background-color: #c4c4c4;
  border-radius: 6px;
  margin: 3px;
  border: 1px solid rgba(0, 0, 0, 0.2);
`
const SDotActive = styled.View`
  background-color: #ffffff;
  height: 18px;
  width: 18px;
  border-radius: 9px;
  margin: 0;
  border: 1px solid rgba(0, 0, 0, 0.2);
`
// STYLED END

function PaginationDot(props) {
  const { active, onClick } = props

  return active ? <SDotActive /> : <SDot />
}

function Pagination(props) {
  const { index, dots, onChangeIndex } = props
  const children = []
  handleClick = (event, index) => {
    onChangeIndex(index)
  }
  for (let i = 0; i < dots; i += 1) {
    children.push(
      <PaginationDot key={i} active={i === index} onClick={handleClick} />
    )
  }

  return <SPaginationContainer>{children}</SPaginationContainer>
}

export default Pagination
