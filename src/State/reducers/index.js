import { combineReducers } from "redux"
import filter from "./filter"
import gallery from "./gallery"
import products from "./products"

function categories(state = [], action) {
  switch (action.type) {
    case "SET_CATEGORIES": {
      return action.payload
    }
    default:
      return state
  }
}

function colors(state = [], action) {
  switch (action.type) {
    case "SET_COLORS": {
      return action.payload
    }
    default:
      return state
  }
}

function availability(state = [], action) {
  switch (action.type) {
    case "SET_AVAILABILITY": {
      return action.payload
    }
    default:
      return state
  }
}

export default combineReducers({
  filter,
  gallery,
  products,
  categories,
  colors,
  availability
})
