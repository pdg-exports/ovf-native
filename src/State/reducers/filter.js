import { FilterTypes as types } from '../types'

export default function(state = {}, action) {
  switch (action.type) {
    case types.ADD_FILTER:
      return Object.assign({}, state, {
        [action.payload.name]: action.payload.value,
      })

    case types.REMOVE_FILTER:
      if (state[action.payload.name]) {
        const newFilter = Object.assign({}, state)
        delete newFilter[action.payload.name]
        return newFilter
      }
      return state

    case types.REPLACE_FILTER: {
      let newFilter
      const set = Object.prototype.hasOwnProperty.call(
        state,
        action.payload.name
      )

      newFilter = Object.assign({}, state, {
        [action.payload.name]: action.payload.value,
      })

      if (set) {
        if (state[action.payload.name] === action.payload.value) {
          newFilter = Object.assign({}, state)
          delete newFilter[action.payload.name]
        }
      }
      return newFilter
    }

    case types.CLEAR_ALL: {
      return {}
    }
    default:
      return state
  }
}
