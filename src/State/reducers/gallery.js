import { GalleryTypes as types } from '../types'

export default function(state = 0, action) {
  switch (action.type) {
    case types.FORWARD_ITEM: {
      if (state < action.payload) return state + 1
      return state
    }
    case types.BACK_ITEM: {
      if (state > 0) return state - 1
      return state
    }
    case types.JUMP_TO: {
      return action.payload
    }

    case types.RESET: {
      return 0
    }

    default:
      return state
  }
}
