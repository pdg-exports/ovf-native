import { ProductsTypes as types } from "../types"

export default function(state = [], action) {
  switch (action.type) {
    case types.SET_PRODUCTS: {
      return action.payload
    }
    case types.RESET_PRODUCTS: {
      return action.payload
    }
    default:
      return state
  }
}
