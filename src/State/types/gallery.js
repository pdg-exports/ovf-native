const GalleryTypes = {
  FORWARD_ITEM: 'FORWARD_ITEM',
  BACK_ITEM: 'BACK_ITEM',
  JUMP_TO: 'JUMP_TO',
  RESET: 'RESET',
}

export default GalleryTypes
