const FilterTypes = {
  ADD_FILTER: 'ADD_FILTER',
  REMOVE_FILTER: 'REMOVE_FILTER',
  REPLACE_FILTER: 'REPLACE_FILTER',
  CLEAR_ALL: 'CLEAR_ALL',
}

export default FilterTypes
