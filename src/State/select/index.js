import { createSelector } from "reselect"
import { inArrayField, extractInArrayField } from "../../Util/ArrayHelpers"

const getProducts = state => state.products
const getFilters = state => state.filter
const getProps = (state, props) => props.navigation.getParam("id", undefined)

export const getFilteredResults = createSelector(
  [getProducts, getFilters],
  (products, filter) => {
    let filteredProducts = [...products]
    if (filter.category) {
      filteredProducts = filteredProducts.filter(
        product =>
          product.flower_category_taxonomy &&
          inArrayField(product.flower_category_taxonomy, filter.category)
      )
    }

    if (filter.color) {
      filteredProducts = filteredProducts.filter(
        product =>
          product.color_taxonomy &&
          inArrayField(product.color_taxonomy, filter.color)
      )
    }

    if (filter.month) {
      filteredProducts = filteredProducts.filter(
        product =>
          product.availability_taxonomy &&
          extractInArrayField(product.availability_taxonomy, filter.month)
      )
    }
    return filteredProducts
  }
)

export const getResultById = createSelector(
  [getProducts, getProps],
  (products, id) => {
    let solo = products.filter(product => product.id === id)
    return { item: solo[0] }
  }
)
