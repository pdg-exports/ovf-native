import { ProductsTypes as types } from '../types'

export function setProducts(products) {
  return { type: types.SET_PRODUCTS, payload: products }
}

export function resetProducts(products) {
  return { type: types.RESET_PRODUCTS, payload: products }
}
