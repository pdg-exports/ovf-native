import { FilterTypes as types } from '../types'

export function addFilter(name, value) {
  return {
    type: types.ADD_FILTER,
    payload: {
      name,
      value,
    },
  }
}

export function removeFilter(name) {
  return {
    type: types.REMOVE_FILTER,
    payload: {
      name,
    },
  }
}

export function replaceFilter(name, value) {
  return {
    type: types.REPLACE_FILTER,
    payload: {
      name,
      value,
    },
  }
}

export function clearAll() {
  return {
    type: types.CLEAR_ALL,
  }
}
