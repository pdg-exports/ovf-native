import { GalleryTypes as types } from '../types'

export function forwardItem(totalCount) {
  return { type: types.FORWARD_ITEM, payload: totalCount }
}

export function backItem() {
  return { type: types.BACK_ITEM }
}

export function jumpTo(index) {
  return { type: types.JUMP_TO, payload: index }
}

export function reset() {
  return { type: types.RESET }
}
