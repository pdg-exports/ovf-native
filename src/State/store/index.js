import { createStore, applyMiddleware, compose } from "redux"
import combinedReducer from "../reducers"
/**
 * @desc Remove in production!!! Logging middleware to work with state.
 */
const logger = store => next => action => {
  console.group(action.type)
  console.info("dispatching", action)
  let result = next(action)
  console.log("next state", store.getState())
  console.groupEnd()
  return result
}

const enhancers = []
const middleware = []

const composedEnhancers = compose(
  applyMiddleware(...middleware),
  ...enhancers
)

const initStore = (state = {}) =>
  createStore(combinedReducer, (intialState = state), composedEnhancers)

export default initStore
