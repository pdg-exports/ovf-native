import React from "react"
import { MaterialCommunityIcons } from "@expo/vector-icons"
import { Theme } from "../Components/Library"
import { StatusBar } from "react-native"
import { ThemeProvider } from "styled-components"
import {
  createAppContainer,
  createStackNavigator,
  createBottomTabNavigator
} from "react-navigation"

import HomeScreen from "./Home"
import ProductsScreen from "./Products"
import ProductSoloScreen from "./ProductSolo"
import InfoScreen from "./Info"

const ProductStack = createStackNavigator(
  {
    Product: ProductsScreen,
    ProductSolo: ProductSoloScreen
  },
  {
    initialRouteName: "Product",
    headerMode: "none",
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: "#000"
      },
      headerTintColor: "#000",
      headerTitleStyle: {
        fontWeight: "bold",
        color: "#000",
        fontFamily: "nunito-bold"
      }
    }
  }
)

const TabNav = createBottomTabNavigator(
  {
    Home: HomeScreen,
    Product: ProductStack,
    Info: InfoScreen
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state
        let IconComponent = MaterialCommunityIcons
        let iconName
        switch (routeName) {
          case "Home":
            iconName = `home`
            break
          case "Product":
            iconName = `flower`
            break
          case "Info":
            iconName = `information`
            break
        }

        return <IconComponent name={iconName} size={36} color={tintColor} />
      }
    }),
    tabBarOptions: {
      showLabel: false,
      activeTintColor: Theme.palette.primary,
      inactiveTintColor: Theme.palette.ltgrey,
      style: { height: 48 }
    }
  }
)

const AppContainer = createAppContainer(TabNav)

const OVFapp = props => {
  return (
    <>
      <StatusBar backgroundColor="#000" barStyle="light-content" />
      <ThemeProvider theme={Theme}>
        <AppContainer />
      </ThemeProvider>
    </>
  )
}

export default OVFapp
