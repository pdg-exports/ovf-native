import React from "react"
import { ImageBackground, Dimensions } from "react-native"
import PADisplay from "../Components/PADisplay"
import PADrawer from "../Components/PADrawer"
import PATabs from "../Components/PATabs"
import { Theme } from "../Components/Library"
import { H2 } from "../Components/Library/Typography"

import bkg from "../../assets/img/backgrounds/flower_pattern.png"

const dimensions = Dimensions.get("window")

export default class ProductsScreen extends React.Component {
  render() {
    const { navigation } = this.props
    return (
      <PADrawer>
        <ImageBackground
          source={bkg}
          style={{
            height: dimensions.height * 0.2,
            justifyContent: "flex-end",
            paddingBottom: 0,
            paddingLeft: 20,
            paddingRight: 20,
            backgroundColor: Theme.palette.secondary
          }}
          resizeMode="cover"
          resizeMethod="scale"
        >
          <H2 color="#fff" align="left" gutterBottom>
            Our Flowers
          </H2>
          <PATabs />
        </ImageBackground>

        <PADisplay navigation={navigation} controller />
      </PADrawer>
    )
  }
}

ProductsScreen.navigationOptions = {
  title: "Product"
}
