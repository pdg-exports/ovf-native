import React from "react"
import { View, Text } from "react-native"
import { Button } from "../Components/Library"

class HomeScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <Text>Home Screen</Text>
        <Button
          color="secondary"
          variant="filled"
          onPress={() => {
            this.props.navigation.navigate("Product")
          }}
        >
          Go to Products Page
        </Button>
      </View>
    )
  }
}

export default HomeScreen
