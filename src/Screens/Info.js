import React from "react"
import { View } from "react-native"
import { H1, H4 } from "../Components/Library/Typography"

export default class InfoScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <H1>Info Screen</H1>
        <H4>This content suppose to change based on user role</H4>
        <H4>
          Roles are: retail florist, supermarkets, wholesalers, average
          consumer.
        </H4>
      </View>
    )
  }
}
