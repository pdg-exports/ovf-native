import React from "react"
import { Text, View, ScrollView, Share } from "react-native"
import { connect } from "react-redux"
import * as Select from "../State/select"
import { decodeHtmlEntity } from "../Util/htmlEntities"
import { MaterialCommunityIcons } from "@expo/vector-icons"
import { H3, P, H2, H4 } from "../Components/Library/Typography"
import PSCarousel from "../Components/PSCarousel"
import PSColor from "../Components/PSColor"
import PSAvailability from "../Components/PSAvailability"
import { Theme } from "../Components/Library"

const ProductSoloScreen = props => {
  const { item, navigation } = props

  onShare = async () => {
    try {
      const result = await Share.share({
        message:
          "This is placeholder message, could be a link or some other stuff"
      })

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message)
    }
  }

  const decodedTitle = decodeHtmlEntity(item.title.rendered)
  return (
    <View>
      <View style={{ height: "50%" }}>
        <PSCarousel variations={item.acf.color_variations} />
        <View style={{ position: "absolute", top: 30, left: 0 }}>
          <MaterialCommunityIcons.Button
            name="arrow-left"
            onPress={() => navigation.navigate("Product")}
            size={36}
            color="#fff"
            borderRadius={0}
            backgroundColor="rgba(255, 255, 255, 0)"
          />
        </View>
        <View style={{ position: "absolute", top: 30, right: 0 }}>
          <MaterialCommunityIcons.Button
            name="share-variant"
            onPress={() => onShare()}
            size={36}
            color="#fff"
            borderRadius={0}
            backgroundColor="rgba(255, 255, 255, 0)"
          />
        </View>
      </View>

      <View style={{ height: "50%" }}>
        <ScrollView>
          <View
            style={{
              paddingLeft: 10,
              paddingRight: 10,
              display: "flex",
              flexDirection: "column"
            }}
          >
            <H2 align="left" gutterBottom>
              {decodedTitle}
            </H2>
            <View
              style={{
                flexDirection: "row",
                flexWrap: "wrap",
                marginBottom: 10
              }}
            >
              <P align="left" color={Theme.palette.primary}>
                {item.acf.botanical_name}
              </P>
              <Text style={{ marginLeft: 2, marginRight: 2 }}>&#124;</Text>
              <P align="left">{item.acf.other_names}</P>
            </View>
            <H4 align="left" gutterBottom>
              COLOR
            </H4>
            <PSColor variations={item.acf.color_variations} />
            {item.availability_taxonomy && (
              <>
                <H4 align="left" gutterBottom>
                  AVAILABILITY
                </H4>
                <PSAvailability avail={item.availability_taxonomy} />
              </>
            )}
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                marginTop: 10
              }}
            >
              {item.acf.stem_length && (
                <View style={{ marginRight: 10 }}>
                  <H4 align="left" gutterBottom>
                    STEM LENGTH
                  </H4>
                  <P align="left" gutterBottom>
                    {item.acf.stem_length}
                  </P>
                </View>
              )}

              {item.acf.flower_length && (
                <View>
                  <H4 align="left" gutterBottom>
                    FLOWER LENGTH
                  </H4>
                  <P align="left" gutterBottom>
                    {item.acf.flower_length}
                  </P>
                </View>
              )}
            </View>
          </View>

          <View style={{ paddingLeft: 10, paddingRight: 10 }}>
            <H3 align="left" gutterBottom>
              Family Roots
            </H3>
            <P align="left" gutterBottom>
              {item.acf.family_roots}
            </P>
            <H3 align="left" gutterBottom>
              Personality
            </H3>
            <P align="left" gutterBottom>
              {item.acf.personality}
            </P>
            <H3 align="left" gutterBottom>
              Care & Handling
            </H3>
            <P align="left" gutterBottom>
              {item.acf.care_handling}
            </P>
            <H3 align="left" gutterBottom>
              Tidbits
            </H3>
            <P align="left" gutterBottom>
              {item.acf.tidbits}
            </P>
          </View>
        </ScrollView>
      </View>
    </View>
  )
}

ProductSoloScreen.navigationOptions = ({ navigation }) => {
  var decodedTitle = decodeHtmlEntity(navigation.getParam("title", "Product"))
  return {
    title: decodedTitle,
    headerMode: "none",
    headerStyle: {
      backgroundColor: "#000"
    },

    headerTitleStyle: {
      color: "#000",
      textTransform: "capitalize"
    }
  }
}

export default connect((state, props) => Select.getResultById(state, props))(
  ProductSoloScreen
)
