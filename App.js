import React from "react"
import { AppLoading } from "expo"
import * as Font from "expo-font"
import { getApiData, pingServer } from "./src/Util/Network"
import { Provider as ReduxProvider } from "react-redux"
import initStore from "./src/State/store"
import OVFapp from "./src/Screens"

export default class App extends React.Component {
  state = {
    isReady: false,
    store: null
  }

  _loadResources = async () => {
    const ping = await pingServer()
    await Font.loadAsync({
      "nunito-bold": require("./assets/fonts/NunitoSans-Bold.ttf"),
      "nunito-semi": require("./assets/fonts/NunitoSans-SemiBold.ttf"),
      "nunito-regular": require("./assets/fonts/NunitoSans-Regular.ttf")
    })
    // gotta love magic numbers
    if (ping) {
      const products = await getApiData(
        "flowers?per_page=100&orderby=slug&order=asc"
      )
      const categories = await getApiData("flower_category")
      const availability = await getApiData("availability?per_page=24")
      const colors = await getApiData("colors?per_page=24")
      // load fonts locally
      return this.setState({
        store: { products, categories, availability, colors }
      })
    }
  }

  render() {
    const { isReady, store } = this.state

    if (!isReady) {
      return (
        <AppLoading
          startAsync={this._loadResources}
          onFinish={() => this.setState({ isReady: true })}
          onError={console.warn}
        />
      )
    }
    return (
      <ReduxProvider store={initStore(store)}>
        <OVFapp />
      </ReduxProvider>
    )
  }
}
